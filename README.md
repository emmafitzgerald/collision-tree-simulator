# README #

Simulator for alarm handling using collision trees in industrial Internet of Things scenarios

## Configuration and Installation ##

First, install numpy if you do not already have it, for example with pip:

```
pip install numpy
```

## Running the simulator and plotting the results ##

Then you can run the simulation with the command

```
python ./main.py
```

The results will be displayed in the console, as well as recorded to .dat filed in the data directory. You can plot the results by changing to the data directory and running the command

```
python ./plot_results.py
```

Plotting the results requires matplotlib to be installed. The figures will be saved to the data/figures/ directory as encapsulated postscript files (.eps) and automatically converted to PDF using epstopdf. This step assumes a Linux environment with epstopdf installed.

### Dependencies ###
numpy
matplotlib
epstopdf

## Classes and Files ##

main.py: The main simulation code
collision_tree.py: Implementation of the collision tree data structure and accompanying functions
alarm.py: Contains the Alarm class, representing an alarm source

## How the simulator works ##

The simulator generates a set of alarms that may trigger during a specified window. It then builds a collision tree for these alarms based on their trigger probabilities. Then the actual simulation is run. 

During the simulation, time proceeds in slots, and in each slot, alarms may be triggered. If a collision occurs, it is resolved using the collision tree. When the end of the window is reached and any ongoing collisions are resolved, statistics are recorded for the simulation run.

For each set of parameters (number of alarms and alarm probability upper bound) a configurable number of scenario instances are generated. Then for each instance, a configurable number of simulation runs are performed. By default, there are 20 instances for each set of parameters, and 50 simulation runs for each instance.

Finally, the simulation statistics are recorded, along with analytical performance measures for the collision tree. The resulting data can be viewed in the data directory, and plotted using the provided script there.

## How to cite this simulator ##

The following BibTeX entry can be used to cite this simulator.

```
@misc{collision_tree,
    howpublished = {\url{https://bitbucket.org/emmafitzgerald/collision-tree-simulator},
    title = {Collision Tree Simulator},
    author = {Emma Fitzgerald}
}
```


## Contact ##

The repository is owned and run by Emma Fitzgerald: emma.fitzgerald@eit.lth.se
