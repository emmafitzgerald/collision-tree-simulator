#!/usr/bin/python

# Class to represent an alarm source
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
import random
import functools

@functools.total_ordering
class Alarm:
    prob_lower_bound = 0.0
    prob_upper_bound = 0.1
    deadline_lower_bound = 5
    deadline_upper_bound = 10
    alarm_id = 0

    def __init__(self, prob=0, deadline=0, prob_ub=-1):
        if prob_ub > 0:
            self.prob_upper_bound = prob_ub
        if prob == 0:
            self.prob = random.uniform(self.prob_lower_bound, self.prob_upper_bound) 
        else:
            self.prob = prob
        
        if deadline == 0:
            self.deadline = random.randint(self.deadline_lower_bound, self.deadline_upper_bound)
        else:
            self.deadline = deadline

        self.id = Alarm.alarm_id
        Alarm.alarm_id += 1
        self.pilot_sequence = []
        self.triggered = False
        self.service_time = -1

    def set_pilot_sequence(self, pilots):
        self.pilot_sequence = pilots

    def get_pilot_sequence(self):
        return self.pilot_sequence

    def trigger(self):
        self.triggered = True

    def triggered(self):
        return self.triggered

    def set_service_time(self, time):
        self.service_time = time

    def get_service_time(self):
        return self.service_time

    def set_expected_service_time(self, time):
        self.expected_service_time = time

    def get_expected_service_time(self):
        return self.expected_service_time

    def __str__(self):
        return str(self.id) + ": (" + str(self.prob) + ", " + str(self.deadline) + ")"

    def __lt__(self, other):
        return self.prob < other.prob

    def __eq__(self, other):
        return self.prob == other.prob

    @staticmethod
    def reset_id():
        Alarm.alarm_id = 0
