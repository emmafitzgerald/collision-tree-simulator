#!/usr/bin/python

# Collision tree for handling alarm traffic in industrial Internet of Things scenarios 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from heapq import *
import functools

import alarm

@functools.total_ordering
class Node:

    def __init__(self, parent=None, alarm=None):
        self.parent = parent
        self.alarm = alarm
        self.children = []
        self.prob = 0.0
        self.pilot = -1
        self.leaves = []

    def set_parent(self, parent):
        self.parent = parent

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        return self.children

    def set_prob(self, prob):
        self.prob = prob

    def get_prob(self):
        return self.prob

    def find_leaves(self):
        leaves = []
        # Copy children list so we don't erase it
        to_process = self.children[:]

        while(len(to_process) > 0):
            node = to_process.pop()
            if node.isleaf():
                leaves.append(node)
            else:
                to_process.extend(node.children)

        self.leaves = leaves

        return leaves

    def set_leaves(self, leaves):
        self.leaves = leaves

    # Make sure to call find_leaves() or coll_tree.find_alarm_subsets() first
    def get_leaves(self):
        return self.leaves

    # Calculate probability based on probability of children
    def calculate_prob(self):
        self.find_leaves()

        prob = 1.0
        for leaf in self.leaves:
            prob *= (1.0 - leaf.prob)

        prob = 1.0 - prob
        self.prob = prob

    def get_alarm(self):
        return self.alarm

    def set_alarm(self, alarm):
        self.alarm = alarm

    def set_pilot(self, pilot):
        self.pilot = pilot

    def get_pilot(self):
        return self.pilot

    def isleaf(self):
        if len(self.children) == 0:
            return True
        else:
            return False

    def __lt__(self, other):
        return self.prob < other.prob
    
    def __eq__(self, other):
        if other == None:
            return False
        return self.prob == other.prob

    def __repr__(self):
        return "(" + str(self.prob) + ", " + str(self.pilot) + ")"

class Collision_Tree:

    def __init__(self, alarms):
        self.build_tree(alarms)
        self.pilots = []

    # alarms: List of alarm.Alarm
    def build_tree(self, alarms):
        nodes = []
        for alarm in alarms:
            node = Node(alarm=alarm)
            node.set_prob(alarm.prob)
            heappush(nodes, node)

        while(True):
            if len(nodes) <= 1:
                break

            node1 = heappop(nodes)
            node2 = heappop(nodes)

            new_node = Node()
            node1.set_parent(new_node)
            node2.set_parent(new_node)
            new_node.add_child(node1)
            new_node.add_child(node2)

            new_node.calculate_prob()

            heappush(nodes, new_node)

        # There should only be one node left
        self.root = heappop(nodes)
        self.find_alarm_subsets()

        if len(nodes) > 0:
            print "Error: leftover nodes after building collision tree"
            for node in nodes:
                print "\t", node

    def assign_pilots(self):
        level = 0
        current_level = []
        current_level.append(self.root)
        next_level = []
        self.pilots = []

        while(len(current_level) > 0):
            self.pilots.append(len(current_level))
            pilot = 0
            for node in current_level:
                node.set_pilot(pilot)
                pilot += 1
                next_level.extend(node.children)
            current_level = next_level
            next_level = []
            level += 1

        self.create_pilot_sequences()

        return self.pilots[:]

    def create_pilot_sequences(self, node=None, sequence=None):
        if node == None:
            node = self.root
        if sequence == None:
            sequence = []

        sequence.append(node.get_pilot())

        for child in node.children:
            self.create_pilot_sequences(node=child, sequence=sequence[:])

        alarm = node.get_alarm()
        if alarm != None:
            alarm.set_pilot_sequence(sequence)

        return sequence

    # Records leaf nodes of each node, i.e. the subset of alarms covered by each node
    def find_alarm_subsets(self, node=None):
        if node == None:
            node = self.root

        leaves = []

        for child in node.children:
            child_leaves = self.find_alarm_subsets(node=child)
            leaves.extend(child_leaves)

        node.set_leaves(leaves)

        if len(leaves) == 0:
            return [node]

        return leaves

    def resolve_collision(self, alarms):
        # Number of pilots needed in each subsequent slot to resolve this collision
        pilots_needed = []
        current_level = []
        current_level.append(self.root)
        next_level = []
        level = 0

        alarm_dict = {}
        for alarm in alarms:
            alarm_dict[alarm.id] = alarm

        # Walk collision tree with breadth-first search
        while(len(current_level) > 0):
            # Add number of nodes at this level to number of pilots needed
            pilots_needed.append(len(current_level))

            # Check for collisions
            for node in current_level:
                count = 0
                collision = False
                for leaf in node.leaves:
                    alarm = leaf.get_alarm().id
                    # All leaf nodes should have alarms, so alarm should not be None
                    if alarm in alarm_dict:
                        count += 1

                    if count > 1:
                        # Collision
                        collision = True
                        # Add child nodes to next level for search
                        next_level.extend(node.children)
                        # Don't need to know how many are in the collision, once we get to 2 we are done
                        break

                # For nodes with no collisions, record alarm service times
                if not collision:
                    if node.isleaf():
                        alarm = node.get_alarm().id
                        if alarm in alarm_dict:
                            alarm_dict[alarm].set_service_time(level + 1)

                    for leaf in node.leaves:
                        alarm = leaf.get_alarm().id
                        if alarm in alarm_dict:
                            alarm_dict[alarm].set_service_time(level + 1)

            current_level = next_level
            next_level = []
            level += 1

        return pilots_needed 

    def collision_prob(self, node):
        only_one = 0.0
        for leaf in node.leaves:
            p = leaf.get_prob()
            for leaf2 in node.leaves:
                if leaf2.get_alarm().id == leaf.get_alarm().id:
                    continue
                p *= 1.0 - leaf2.get_prob()
            only_one += p

        all_silent = 1.0
        for leaf in node.leaves:
            all_silent *= 1.0 - leaf.get_prob()

        coll_prob = 1.0 - only_one - all_silent
        return coll_prob

    def expected_pilots_recurse(self, node):
        exp_pilots = 0.0

        for child in node.children:
            exp_pilots += self.expected_pilots_recurse(child)

        exp_pilots += self.collision_prob(node.parent)

        return exp_pilots

    def calculate_expected_num_pilots(self):
        exp_pilots = 1.0    # root node
        
        for node in self.root.children:
            exp_pilots += self.expected_pilots_recurse(node)

        return exp_pilots

    def conditional_collision_prob(self, node, alarm):
        prod = 1.0
        for leaf in node.leaves:
            if leaf.get_alarm().id != alarm.id:
                prod *= 1.0 - node.get_prob()
        return 1.0 - prod

    def calculate_expected_service_times(self):
        leaves = self.root.get_leaves()

        for leaf in leaves:
            exp_service_time = 1
            alarm = leaf.get_alarm()
            node = leaf.parent
            # Exclude leaf node since we use its pilot unconditionally
            while node.parent != None:
                exp_service_time += self.conditional_collision_prob(node, alarm)
                node = node.parent
            alarm.set_expected_service_time(exp_service_time)
    
    def print_tree(self, node=None, level=0):
        if node == None:
            node = self.root

        if node.alarm != None:
            alarm_string = " (" + str(node.get_alarm().id) + ")"
            if node.alarm.deadline <= level + 1:
                alarm_string += "!"
        else:
            alarm_string = ""

        if level == 0:
            print repr(node) + alarm_string
        else:
            print "\t" * (level - 1) + "|---" + repr(node) + alarm_string
        for child in node.children:
            self.print_tree(node=child, level=level+1)
