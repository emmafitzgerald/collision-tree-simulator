#!/usr/bin/python

# Main simulation code for alarm handling in industrial Internet of Things scenarios 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import random
import sys
import numpy
import math
import os
from os import path

from alarm import Alarm
import collision_tree

class Run_Stats:
    num_vals = 9

    def __init__(self, run, num_alarms_triggered, total_reserved_pilots, max_pilots_per_slot, num_collisions,
            pilots_for_collision_res, num_transmissions, pilots_per_slot, avg_service_time, max_service_time):
        self.run = run
        self.num_alarms_triggered = num_alarms_triggered
        self.total_reserved_pilots = total_reserved_pilots
        self.max_pilots_per_slot = max_pilots_per_slot
        self.num_collisions = num_collisions
        self.pilots_for_collision_res = pilots_for_collision_res
        self.num_transmissions = num_transmissions
        self.pilots_per_slot = pilots_per_slot
        self.avg_service_time = avg_service_time
        self.max_service_time = max_service_time

        self.stats = [self.num_alarms_triggered,
                        self.total_reserved_pilots,
                        self.max_pilots_per_slot,
                        self.num_collisions,
                        self.pilots_for_collision_res,
                        self.num_transmissions,
                        self.pilots_per_slot,
                        self.avg_service_time,
                        self.max_service_time]

    def output(self):
        outstring = str(self.run) + "\t"
        first = True
        for stat in self.stats:
            if not first:
                outstring += "\t"
            first = False
            outstring += str(stat)

        return outstring

    def get_values(self):
        for stat in self.stats:
            yield float(stat)

def create_alarms(num_alarms=20, prob_ub=-1):
    alarms = []

    for i in xrange(num_alarms):
        a = Alarm(prob_ub=prob_ub)
        alarms.append(a)

    return alarms

def run_simulation(alarms, coll_tree, window=50, run=0):
    available_alarms = alarms[:]
    next_alarms = []
    processed_alarms = []
    total_reserved_pilots = 1 * window
    collisions = []
    next_collisions = []
    max_pilots_per_slot = 1
    num_collisions = 0
    num_transmissions = 0

    for slot in xrange(window):
        print "Slot", slot
        triggered_alarms = []

        pilots_this_slot = 1
        # Allocate pilots for ongoing collisions
        for coll in collisions:
            alarms = coll[0]
            pilots_needed = coll[1]
            index = coll[2]
            pilots_this_slot += pilots_needed[index]
            if index < len(pilots_needed) - 1:
                # Continue in next slot
                next_collisions.append([alarms, pilots_needed, index + 1])
            else:
                processed_alarms.extend(alarms)

            if pilots_this_slot > max_pilots_per_slot:
                max_pilots_per_slot = pilots_this_slot
    
        # Check for newly triggered alarms
        for alarm in available_alarms:
            if random.uniform(0.0, 1.0) < alarm.prob:
                print "Alarm", alarm.id, "triggered"
                triggered_alarms.append(alarm)
                alarm.trigger()
            else:
                next_alarms.append(alarm)

        if len(triggered_alarms) > 0:
            num_transmissions += 1

        # Resolve collisions or move alarm to processed if no collision
        if len(triggered_alarms) > 1:
            print "Collision!",
            num_collisions += 1
            pilots_needed = coll_tree.resolve_collision(triggered_alarms) 
            pilot_sum = 0
            for p in pilots_needed:
                pilot_sum += p
            pilot_sum -= 1  # ignore first pilot since it's for the root node, i.e. common pilot
            total_reserved_pilots += pilot_sum
            print pilot_sum, "pilots needed for resolution in total"
            print pilots_needed
            next_collisions.append([triggered_alarms, pilots_needed, 1])
        else:
            print "No collisions"
            processed_alarms.extend(triggered_alarms)
            for alarm in triggered_alarms:
                alarm.set_service_time(1)

        collisions = next_collisions
        next_collisions = []
        available_alarms = next_alarms
        next_alarms = []

    extra_slots = 0
    while(len(collisions) > 0):
        extra_slots += 1
        pilots_this_slot = 1
        # Allocate pilots for ongoing collisions
        for coll in collisions:
            alarms = coll[0]
            pilots_needed = coll[1]
            index = coll[2]
            pilots_this_slot += pilots_needed[index]
            if index < len(pilots_needed) - 1:
                # Continue in next slot
                next_collisions.append([alarms, pilots_needed, index + 1])
            else:
                processed_alarms.extend(alarms)

            if pilots_this_slot > max_pilots_per_slot:
                max_pilots_per_slot = pilots_this_slot

        collisions = next_collisions
        next_collisions = []

    pilots_per_slot = float(total_reserved_pilots) / float(window + extra_slots)
    print "Simulation ended"

    avg = 0.0
    max_time = 1
    for alarm in reversed(sorted(processed_alarms)):
        time = alarm.get_service_time()
        print "Alarm", alarm.id, "service time", time
        avg += float(time)
        if time > max_time:
            max_time = time
    if len(processed_alarms) > 0:
        avg /= float(len(processed_alarms))
    else:
        avg = 1.0

    print "Number of slots:", window + extra_slots
    print "Number of alarms triggered:", len(processed_alarms)
    print "Total number of pilots reserved for alarms:", total_reserved_pilots
    print "Max number of pilots reserved in any slot:", max_pilots_per_slot
    print "Number of collisions:", num_collisions
    print "Number of pilots for collision resolution:", total_reserved_pilots - window
    print "Number of slots with alarm transmissions:", num_transmissions
    print "Number of pilots reserved per slot:", pilots_per_slot
    print "Average alarm service time:", avg
    print "Max alarm service time:", max_time

    stats = Run_Stats(run, len(processed_alarms), total_reserved_pilots, max_pilots_per_slot, num_collisions,
            total_reserved_pilots - window, num_transmissions, pilots_per_slot, avg, max_time)
    return stats

def average_service_time(alarms):
    time = 0.0
    for alarm in alarms:
        time += alarm.get_expected_service_time()
    time /= len(alarms)

    return time

# Get average and confidence interval delta of a vector of values
def process_vector(v):
    if len(v) == 0:
        print "Empty vector"
        return (-1, -1) 
    avg = numpy.mean(v)
    stddev = numpy.std(v)
    n = len(v)
    delta = 1.96 * (stddev / math.sqrt(n))
    return (avg, delta)

def create_result_string(results):
    result_string = ""
    count = 0
    for res in results:
        count += 1
        avg, delta = process_vector(res)
        result_string += "\t" + str(avg) + "\t" + str(delta)
    return result_string, count

def sim_header():
    header = "# Run\tNum alarms triggered\tTotal reserved pilots\tMax pilots per slot\tNum collisions\tPilots for coll res\t"
    header += "Num transmissions\tPilots per slot\tAvg service time\tMax service time"
    return header

def aggr_header():
    header = "# Instance\tNum alarms triggered\tdelta\tTotal reserved pilots\tdelta\tMax pilots per slot\tdelta\t"
    header += "Num collisions\tdelta\tPilots for coll res\tdelta\t"
    header += "Num transmissions\tdelta\tPilots per slot\tdelta\tAvg service time\tdelta\tMax service time\tdelta"
    header += "Analytical avg service time\tExpected pilots per slot"
    return header

def config_aggr_header():
    header = "# Num alarms\tProb upper bound\tNum alarms triggered\tdelta\tTotal reserved pilots\tdelta\t"
    header += "Max pilots per slot\tdelta\tNum collisions\tdelta\tPilots for coll res\tdelta\t"
    header += "Num transmissions\tdelta\tPilots per slot\tdelta\tAvg service time\tdelta\tMax service time\tdelta\t"
    header += "Analytical avg service time\tdelta\tExp num pilots per slot\tdelta\tExp - avg num pilots per slot\tdelta"
    return header

def generate_configs():
    for num_alarms in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
        for prob_upper_bound in [0.001, 0.005, 0.01, 0.05, 0.1, 0.5]:
            yield [num_alarms, prob_upper_bound]

if __name__ == "__main__":
    data_dir = "../data/"
    # random uses current time to initialise the seed but we want to save it, so
    # explicitly get a seed
    seed = random.randrange(sys.maxsize)
    random.seed(seed)
    print "Seed:", seed

    num_runs = 50
    num_instances = 20
    window = 50
    prob_upper_bound = 0.1
    num_alarms = 100

    config_aggr_file = open(data_dir + "all_configs.dat", "w") 
    config_aggr_file.write(config_aggr_header() + "\n")


    for config in generate_configs():
        config_vectors = []
        for i in xrange(Run_Stats.num_vals + 3):
            config_vectors.append([])
            
        num_alarms = config[0]
        prob_upper_bound = config[1]
        
        config_dir = str(num_alarms) + "_" + str(prob_upper_bound) + "/"
        if not os.path.exists(data_dir + config_dir):
            os.mkdir(data_dir + config_dir)

        aggr_file = open(data_dir + config_dir + "aggregated_results.dat", "w")
        aggr_file.write(aggr_header() + "\n")

        for instance in xrange(num_instances):
            print "Instance", instance
            Alarm.reset_id()
            alarms = create_alarms(num_alarms=num_alarms, prob_ub=prob_upper_bound)

            coll_tree = collision_tree.Collision_Tree(alarms)
            pilots = coll_tree.assign_pilots()
            coll_tree.create_pilot_sequences()
            coll_tree.print_tree()
            coll_tree.calculate_expected_service_times()
            exp_num_pilots = coll_tree.calculate_expected_num_pilots()

            time = average_service_time(alarms)

            max_exceeds_dl = 0
            exp_exceeds_dl = 0
            for alarm in alarms:
                if len(alarm.get_pilot_sequence()) > alarm.deadline:
                    max_exceeds_dl += 1
                if alarm.get_expected_service_time() > alarm.deadline:
                    exp_exceeds_dl += 1

            for alarm in alarms:
                print "Alarm " + str(alarm.id) + ": " + str(alarm.get_pilot_sequence()) + ", " + str(alarm.deadline)
                print "\tExpected service time:", alarm.get_expected_service_time()

            vectors = []
            for i in xrange(Run_Stats.num_vals):
                vectors.append([])

            sim_data = open(data_dir + config_dir + str(instance) + ".dat", "w")
            sim_data.write(sim_header() + "\n")
            for run in xrange(num_runs):
                stats = run_simulation(alarms, coll_tree, run=run)
                sys.stdout.flush()
                i = 0
                for val in stats.get_values():
                    vectors[i].append(val)
                    i += 1
                sim_data.write(stats.output() + "\n")
            sim_data.close()

            print "Creating result string for instance", instance
            res_str, count = create_result_string(vectors)
            aggr_file.write(str(instance) + res_str)

            avg_num_pilots, delta = process_vector(vectors[6])
            diff = exp_num_pilots - avg_num_pilots

            aggr_file.write("\t" + str(time) + "\t" + str(exp_num_pilots) + "\n")

            print "Weighted expected service time:", time
            print "Number of alarms whose max service time exceeds the deadline:", max_exceeds_dl
            print "Number of alarms who expected service time exceeds the deadline:", exp_exceeds_dl
            print "A priori expected number of pilots reserved per slot:", exp_num_pilots
            print "Diff:", diff

            i = 0
            for vector in vectors:
                avg, delta = process_vector(vector)
                config_vectors[i].append(avg)
                i += 1
            config_vectors[-3].append(time)
            config_vectors[-2].append(exp_num_pilots)
            config_vectors[-1].append(diff)

        aggr_file.close()

        print "Creating result string for config", config

        config_res_str, count = create_result_string(config_vectors)
        config_aggr_file.write(str(num_alarms) + "\t" + str(prob_upper_bound) + "\t" + config_res_str + "\n")

    config_aggr_file.close()
    
    print "Done."
