#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use("Agg")
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt 
import os

eps = 0.000001

red = "#FF6666"
blue = "#CCCCFF"
green = "#003300"

fields = { "num_alarms" : 0,
            "prob_upper_bound" : 1,
            "num_alarms_triggered" : 2,
            "num_alarms_triggered_delta" : 3,
            "total_reserved_pilots" : 4,
            "total_reserved_pilots_delta" : 5,
            "max_pilots_per_slot" : 6,
            "max_pilots_per_slot_delta" : 7,
            "num_collisions" : 8,
            "num_collisions_delta" : 9,
            "pilots_for_coll_res" : 10,
            "pilots_for_coll_res_delta" : 11,
            "num_transmissions" : 12,
            "num_transmissions_delta" : 13,
            "pilots_per_slot" : 14,
            "pilots_per_slot_delta" : 15,
            "avg_service_time" : 16,
            "avg_service_time_delta" : 17,
            "max_service_time" : 18,
            "max_service_time_delta" : 19,
            "weighted_service_time" : 20,
            "weighted_service_time_delta" : 21,
            "exp_num_pilots_per_slot" : 22,
            "exp_num_pilots_per_slot_delta" : 23,
            "exp_minus_avg_pilots_per_slot" : 24,
            "exp_minus_avg_pilots_per_slot_delta" : 25 }

def pilots_per_slot(data, filename, prob=0.1):
    fig, ax = plt.subplots()

    i = fields["prob_upper_bound"]
    idx = np.abs(data[:,i] - prob) < eps

    i = fields["num_alarms"]
    x = data[idx,i]

    i = fields["pilots_per_slot"]
    y_avg = data[idx, i]
    err_avg = data[idx, i+1]

    i = fields["max_pilots_per_slot"]
    y_max = data[idx, i]
    err_max = data[idx, i+1]

    i = fields["exp_num_pilots_per_slot"]
    y_exp = data[idx, i]
    err_exp = data[idx, i+1]

    plt.errorbar(x, y_avg, yerr=err_avg, label="simulation average")
    plt.errorbar(x, y_max, yerr=err_max, label="simulation max")
    plt.errorbar(x, y_exp, yerr=err_exp, label="analysis")

    plt.xlim(left=20)
    plt.ylim(bottom=0)
    plt.xlabel("number of alarm sources")
    plt.ylabel("pilots reserved per slot")
    plt.legend()

    plt.savefig(filename)
    plt.cla()


def service_time(data, filename, prob=0.01):
    fig, ax = plt.subplots()

    i = fields["prob_upper_bound"]
    idx = np.abs(data[:,i] - prob) < eps

    i = fields["num_alarms"]
    x = data[idx,i]

    i = fields["avg_service_time"]
    y_avg = data[idx, i]
    err_avg = data[idx, i+1]

    i = fields["max_service_time"]
    y_max = data[idx, i]
    err_max = data[idx, i+1]

    i = fields["weighted_service_time"]
    y_exp = data[idx, i]
    err_exp = data[idx, i+1]

    plt.errorbar(x, y_avg, yerr=err_avg, label="simulation average")
    plt.errorbar(x, y_max, yerr=err_max, label="simulation max")
    plt.errorbar(x, y_exp, yerr=err_exp, label="analysis")

    plt.xlim(left=20)
    plt.ylim(bottom=0)
    plt.xlabel("number of alarm sources")
    plt.ylabel("alarm delivery time (slots)")
    plt.legend()

    plt.savefig(filename)
    plt.cla()

data_dir = "./"
data = np.genfromtxt(data_dir + "all_configs.dat")

os.chdir("../figures/")

for prob in [0.001, 0.005, 0.01, 0.05, 0.1, 0.5]:
    service_time(data, "delivery_time_" + str(prob) + ".eps", prob=prob)

for prob in [0.001, 0.005, 0.01, 0.05, 0.1, 0.5]:
    pilots_per_slot(data, "pilots_per_slot_" + str(prob) + ".eps", prob=prob)

os.system("for i in ./*.eps; do epstopdf $i; done")
os.chdir("..")
